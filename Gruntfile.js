/*

TO DO

1) Reduce CSS duplication
   - Ideally just a single build - global.scss turns into /build/global.css
   - Can Autoprefixer output minified?
   - If it can, is it as good as cssmin?
   - Could Sass be used again to minify instead?
   - If it can, is it as good as cssmin?

2) Better JS dependency management
   - Require js?
   - Can it be like the Asset Pipeline where you just do //= require "whatever.js"

3) Is HTML minification worth it?

4) Set up a Jasmine test just to try it.

5) Can this Gruntfile.js be abstracted into smaller parts?
   - https://github.com/cowboy/wesbos/commit/5a2980a7818957cbaeedcd7552af9ce54e05e3fb

*/


module.exports = function(grunt) {

  // Utility to load the different option files
  // based on their names
  function loadConfig(path) {
    var glob = require('glob');
    var object = {};
    var key;

    glob.sync('*', {cwd: path}).forEach(function(option) {
      key = option.replace(/\.js$/,'');
      object[key] = require(path + option);
    });

    return object;
  }

  // Initial config
  var config = {
    pkg: grunt.file.readJSON('package.json')
  }

   grunt.initConfig({
      concat: {
        basic_and_extras: {
          files: {
            'dist/assets/js/libs/libs.min.js': 'app/js/libs/{,*/}*.js',
            'dist/assets/js/modernizer/modernizr.min.js': 'app/js/modernizer/modernizr.custom.js',
            'dist/assets/js/vendor/vendor.js': [
              'app/js/vendor/{,*/}*.js',
              //'app/js/vendor/jquery.slicknav.js'
              //'app/js/vendor/bxlider.js'
            ],
            'dist/assets/js/vendor/filter.js': 'app/js/vendor/jquery.mixitup.min.js',
            'dist/assets/js/global.js': ['app/js/global.js']
          }
        }
      },

      uglify: {
        my_target: {
          files: {
            'dist/assets/js/libs/libs.min.js': ['app/js/libs/{,*/}*.js'],
            'dist/assets/js/modernizer/modernizr.min.js': 'app/js/modernizer/modernizr.custom.js',
            'dist/assets/js/global.min.js': 'dist/assets/js/global.js'
          }
        }
      },
      
      compassMultiple: {
        options : {
          // if you need, you can set options. 
          //environment: 'app',
          //outputStyle: 'dist',
          javascriptsDir: 'dist/assets/js',
          imagesDir: 'dist/assets/img',
          fontsDir: 'dist/assets/fonts',
          //importPath: './css/framework',
          relativeAssets: true,
          time: true
        },
     
        // you can specify compiling target as options.sassDir, and output dir as options.cssDir. 
        // At now, you can only set sassDir and cssDir options. 
        common : {
          options: {
            // every compile needs sassDir and cssDir. 
            sassDir: 'app/sass',
            cssDir: 'dist/assets/css'
          }
        }
      },

      cssmin: {
        target: {
          files: [{
            expand: true,
            cwd: 'dist/assets/css',
            src: /*['*.css', '!*.min.css'],*/'main.css', 
            dest: 'dist/assets/css',
            ext: '.min.css'
          }]
        }
      },
  
      imagemin: {
        dynamic:{     
            files: [{
              expand: true,                  // Enable dynamic expansion
              cwd: 'app/img',                // Src matches are relative to this path
              src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
              dest: 'dist/assets/img'        // Destination path prefix
            }]
          }
      },

      assemble: {
          options: {
              data: 'app/template/data/*.json', 
              flatten: true,
              published: false, 
              assetsimg: 'assets/img',
              assetsjs: 'assets/js',
              assetscss: 'assets/css',
              layout: 'app/template/layouts/default.hbs',
              partials: ['app/template/partials/*.hbs']  
          },
          pages: {
              files: {
                  'dist/': ['app/template/pages/*.hbs', '!app/templates/pages/index.hbs']
              }
          },
          index: {
              files: {
                  'dist/': ['app/template/pages/index.hbs']
              }
          }
      },

      //Reformatting HTML 
      prettify: {
          options: {
            indent: 4
            /*indent_char: ' ',
            //wrap_line_length: 78,*/
            //brace_style: 'expand',
            //unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u']
          },
          all: {
              expand: true,
              cwd: 'dist/',
              ext: '.html',
              src: ['*.html'],
              dest: 'dist/'
          }
      },

      // Syncronize Files
      sync: {
          fonts: {
              files: [
                  {
                      cwd: 'app/fonts',
                      src: ['**'],
                      dest: 'dist/assets/fonts'
                  }
              ],
              verbose: true,
              updateAndDelete: true
          },
          images: {
              files: [
                  {
                      cwd: 'app/img',
                      src: ['**'],
                      dest: 'dist/assets/img'
                  }
              ],
              verbose: true,
              updateAndDelete: true
          }
      },


      connect: {
        server: {
          options: {
            port: 9001,
            hostname: 'localhost'
          }
        }
      },

      watch: {
        options: {
          livereload: true,
        },
         //compass: {
           //   files: ['app/sass/{,*/}*.{scss,sass}'],
             // tasks: ['compass:server']
          //},
          uglify: {
              files: ['app/js/libs/{,*/}*.js'],
              tasks: ['uglify']
          },
          concat: {
            files: ['app/js/{,*/}*.js'],
            tasks: ['concat:basic_and_extras']
          },
          //*autoprefixer: {
            //files: ['app/sass/partials/{,*/}*.{sass, scss}'],
            //tasks: ['autoprefixer:dist']
          //},
          imagemin: {
            files: ['{,*/}*.{png,jpg,jpeg}'],
            tasks: ['imagemin:dynamic']
          },
          assemble: {
            files: ['app/template/{,*/}*.hbs', 'app/template/data/{,*/}*.yml', 'app/template/data/{,*/}*.json'],
            tasks: ['assemble',  'prettify:all']
          },
          fonts: {
            files: ['app/fonts/**'],
            tasks: ['sync:fonts']
          },
          compassMultiple: {
            files: ['app/sass/{,*/}*.{scss,sass}'],
            tasks: ['compassMultiple:common']
          },
          cssmin: {
            files: ['dist/assets/css/*.css'],
            tasks: ['cssmin']
          },
          image: {
              files: ['app/img/**'],
              tasks: ['sync:images']
          }
      }

   });



  // Load tasks from the tasks folder
  //grunt.loadTasks('tasks');
  // Load all the tasks options in tasks/options base on the name:
  // watch.js => watch{}
  //grunt.util._.extend(config, loadConfig('./tasks/options/watch{}'));
  
  //grunt.initConfig(config);
  //require('load-grunt-tasks')(grunt);

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  //grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  //grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('assemble');
  grunt.loadNpmTasks('grunt-prettify');
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-compass-multiple');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default Task is basically a rebuild
  grunt.registerTask('default', ['assemble', 'compassMultiple', 'cssmin', 'imagemin', 'concat', 'uglify', 'prettify:all', 'sync', 'connect', 'watch']);
  // Moved to the tasks folder:
  //grunt.registerTask('dev', ['connect', 'watch']);

};
